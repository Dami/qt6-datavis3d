Document: qt6-datavis3d
Title: Debian qt6-datavis3d Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-datavis3d is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-datavis3d/qt6-datavis3d.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-datavis3d/qt6-datavis3d.ps.gz

Format: text
Files: /usr/share/doc/qt6-datavis3d/qt6-datavis3d.text.gz

Format: HTML
Index: /usr/share/doc/qt6-datavis3d/html/index.html
Files: /usr/share/doc/qt6-datavis3d/html/*.html
